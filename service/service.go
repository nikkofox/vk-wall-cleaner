package service

import (
	"gitlab.com/nikkofox/vk-wall-cleaner/ui"
)

// Start configures application and init global App
func Start(version string) {
	ui.InitApp(version)
	ui.App.UI.Start()
}
