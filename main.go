package main

import "gitlab.com/nikkofox/vk-wall-cleaner/service"

const version = "0.5.3"

func main() {
	service.Start(version)
}
