package ui

import (
	"fmt"
	"unicode"

	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
	"gitlab.com/nikkofox/vk-wall-cleaner/core"
)

// TokenPane displays and manages tokens
type TokenPane struct {
	*tview.Flex
	list     *tview.List
	newToken *tview.InputField
}

// newTokenPane initializes and configures TokenPane
func newTokenPane(config *core.Config) *TokenPane {
	pane := TokenPane{
		Flex:     tview.NewFlex().SetDirection(tview.FlexRow),
		list:     tview.NewList(),
		newToken: tview.NewInputField(),
	}

	// TokenPane setup
	savedTokens := config.VK.Tokens
	config.VK.Tokens = nil // reset the saved tokens, core.VK will add them again
	pane.configureList(savedTokens)
	pane.configureTokenField()

	pane.
		AddItem(blank, 2, 0, false).
		AddItem(pane.newToken, 3, 0, true).
		AddItem(pane.list, 0, 2, false).
		AddItem(makeTokenHelp(), 1, 0, false)

	pane.SetBorder(true).SetTitle("[::u]A[::-]ccess Tokens")
	return &pane
}

func makeTokenHelp() *tview.Grid {
	return tview.NewGrid().
		SetColumns(0, 0).
		SetRows(0).
		AddItem(tview.NewTextView().SetText("Удалить токен: Del / d").SetTextAlign(tview.AlignCenter), 0, 0, 1, 1, 0, 0, false).
		AddItem(tview.NewTextView().SetText("Добавить токен: Ins / i").SetTextAlign(tview.AlignCenter), 0, 1, 1, 1, 0, 0, false)
}

// New Token Input field

func (pane *TokenPane) configureTokenField() {
	pane.newToken.SetLabel("Токен пользователя: ").
		SetLabelColor(labelFrontColor).
		SetPlaceholder(" ").SetPlaceholderStyle(customStyle.Background(inactiveBackColor)).
		SetFieldStyle(customStyle.Foreground(brightFrontColor).Background(activeBackColor)).
		SetMaskCharacter('*').
		SetDoneFunc(pane.handleTokenDone).SetChangedFunc(pane.handleTokenChanged)
}

func (pane *TokenPane) handleTokenChanged(_ string) {
	pane.newToken.SetFieldStyle(customStyle.Foreground(brightFrontColor).Background(activeBackColor))
}

func (pane *TokenPane) handleTokenDone(key tcell.Key) {
	{
		switch key {
		case tcell.KeyEnter:
			token := pane.newToken.GetText()
			if len(token) < 100 {
				errorInInputField(pane.newToken, 5)
				App.UI.status.showForSeconds("[red::b]Некорректный формат токена", 5)
				return
			}

			pane.addTokenToList(token)
			pane.newToken.SetText("")
			App.UI.status.showForSeconds("[green::]Токен пользователя успешно добавлен", 5)
		case tcell.KeyEsc, tcell.KeyDown, tcell.KeyTab:
			App.UI.TView.SetFocus(pane.list)
		}
	}
}

func (pane *TokenPane) setList(tokens ...string) {
	for _, t := range tokens {
		pane.addTokenToList(t)
	}
}

func (pane *TokenPane) addTokenToList(token string) *tview.List {
	at := App.VK.AddAccessToken(token)
	return pane.list.AddItem(at.Alias, at.Id, 0, func() {
		pane.selectToken(at.Id)
	})
}

// selectToken marks a token as currently active and loads the walls
func (pane *TokenPane) selectToken(tokenId string) {
	App.UI.status.showForSeconds(
		fmt.Sprintf("Выбран токен: %s", App.VK.ActivateAccessToken(tokenId).Alias), 3,
	)
	App.UI.mainPane.walls.loadWalls()
}

// List Tokens

func (pane *TokenPane) configureList(tokens []string) {
	pane.list.ShowSecondaryText(false).SetInputCapture(pane.handleListShortcuts).SetBorder(true)
	pane.setList(tokens...)
}

func (pane *TokenPane) removeCurrentToken() *tview.List {
	if pane.list.GetItemCount() == 0 {
		return pane.list
	}
	idx := pane.list.GetCurrentItem()
	_, tokenId := pane.list.GetItemText(idx)
	App.UI.status.showForSeconds(fmt.Sprintf(
		"[yellow::]Токен удален: %s [%s]", App.VK.RemoveAccessToken(tokenId).Alias, tokenId,
	), 5)
	return pane.list.RemoveItem(idx)
}

func (pane *TokenPane) handleListShortcuts(event *tcell.EventKey) *tcell.EventKey {
	switch unicode.ToLower(event.Rune()) {
	case 'j':
		pane.list.SetCurrentItem(pane.list.GetCurrentItem() + 1)
		return nil
	case 'k':
		pane.list.SetCurrentItem(pane.list.GetCurrentItem() - 1)
		return nil
	case 'd':
		pane.removeCurrentToken()
		return nil
	case 'i':
		App.UI.TView.SetFocus(pane.newToken)
		return nil
	}

	switch event.Key() {
	case tcell.KeyTab, tcell.KeyRight:
		App.UI.TView.SetFocus(App.UI.mainPane.walls)
		return nil
	case tcell.KeyEsc, tcell.KeyInsert, tcell.KeyBacktab:
		App.UI.TView.SetFocus(pane.newToken)
		return nil
	case tcell.KeyDelete:
		pane.removeCurrentToken()
		return nil
	}
	return event
}
