package ui

import (
	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
	"gitlab.com/nikkofox/vk-wall-cleaner/core"
)

// Name of page keys
const (
	mainPage = "main"
	logPage  = "log"
)

// UserInterface contains elements to control UI of the app
type UserInterface struct {
	TView      *tview.Application
	layout     *tview.Flex
	status     *StatusBar
	mainPane   *mainPane
	ctxWrapper *core.ContextWrapper
	config     *core.Config
}

// newUI creates new UserInterface and all dependent interface elements
func newUI(version string, config *core.Config) *UserInterface {
	app := tview.NewApplication()
	_ = config.LoadConfig()

	status := newStatusBar()
	title := newTitleBar(version) // No need to be managed in realtime, so it's not in the UI

	// Main panels
	main := newMainPane(config)

	ui := &UserInterface{
		TView:      app,
		layout:     createAppLayout(title, main.Pages, status),
		status:     status,
		mainPane:   main,
		ctxWrapper: core.NewContextWrapper(),
		config:     config,
	}
	ui.TView.SetInputCapture(ui.handleGlobalShortcuts)
	return ui
}

func (ui *UserInterface) handleGlobalShortcuts(event *tcell.EventKey) *tcell.EventKey {
	switch event.Key() {
	case tcell.KeyCtrlA:
		ui.TView.SetFocus(ui.mainPane.tokens)
		return nil
	case tcell.KeyCtrlW:
		ui.TView.SetFocus(ui.mainPane.walls)
		return nil
	case tcell.KeyCtrlC:
		ui.Shutdown()
		return nil
	case tcell.KeyCtrlX:
		ui.status.showForSeconds("[red]Получен сигнал на прерывание выполнения", 5)
		ui.ctxWrapper.ToCancelWithReset()
		return nil
	case tcell.KeyCtrlS:
		ui.saveConfig()
		return nil
	case tcell.KeyCtrlR:
		ui.resetConfig()
		return nil
	}
	return event
}

// Start sets the layout to fullscreen and starts the UI
func (ui *UserInterface) Start() {
	if err := ui.TView.SetRoot(ui.layout, true).EnableMouse(true).Run(); err != nil {
		panic(err)
	}
}

// Shutdown stops UI
func (ui *UserInterface) Shutdown() {
	ui.TView.Sync()
	ui.TView.Stop()
}

// saveConfig calls method to save config and catch errors
func (ui *UserInterface) saveConfig() {
	if err := ui.config.SaveConfig(); err != nil {
		ui.status.showForSeconds("[red]Ошибка сохранения настроек", 5)
		return
	}
	ui.status.showForSeconds("[green]Настройки успешно сохранены", 5)
}

// resetConfig calls method to reset config and catch errors
func (ui *UserInterface) resetConfig() {
	if err := ui.config.ResetConfig(); err != nil {
		ui.status.showForSeconds("[red]Ошибка сброса настроек", 5)
	}
	ui.status.showForSeconds("[green]Настройки успешно сброшены, [::b]перезапустите приложение", 5)
}

// hideMainPane allows to hide the main panels and show only logs
func (ui *UserInterface) hideMainPane() {
	ui.mainPane.SwitchToPage(logPage)
}

// hideMainPane allows to hide the log panel and show main panels
func (ui *UserInterface) showMainPane() {
	ui.mainPane.SwitchToPage(mainPage)
}

func createAppLayout(title *tview.Flex, mainPane *tview.Pages, status *StatusBar) *tview.Flex {
	return tview.NewFlex().SetDirection(tview.FlexRow).
		AddItem(title, 2, 0, false).
		AddItem(mainPane, 0, 6, true).
		AddItem(status, 1, 0, false)
}

type mainPane struct {
	*tview.Pages
	layout *tview.Flex
	tokens *TokenPane
	walls  *WallPane
	log    *logField
}

// newMainPane creates the main panel and pages for it
func newMainPane(config *core.Config) *mainPane {
	pane := &mainPane{
		Pages:  tview.NewPages(),
		layout: tview.NewFlex().SetDirection(tview.FlexRow),
		tokens: newTokenPane(config),
		walls:  newWallPane(config),
		log:    newLogField(),
	}
	content := tview.NewFlex().
		AddItem(pane.tokens, 0, 3, true).
		AddItem(pane.walls, 0, 5, false)
	pane.layout.
		AddItem(content, 0, 6, true).
		AddItem(pane.log, 0, 2, false)
	pane.
		AddPage(logPage, pane.log, true, true).
		AddPage(mainPage, pane.layout, true, true)

	return pane
}
