package ui

import (
	"github.com/rivo/tview"
)

func confirmModal(text string, ok func(), cancel func()) *tview.Modal {
	modal := tview.NewModal().
		SetBackgroundColor(modalBackColor).
		SetText(text).
		AddButtons([]string{"Ок", "Отмена"}).
		SetDoneFunc(func(buttonIndex int, buttonLabel string) {
			if buttonIndex == 0 {
				ok()
			} else {
				cancel()
			}
		})
	return modal
}
