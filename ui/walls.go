package ui

import (
	"context"
	"fmt"
	"unicode"

	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
	"gitlab.com/nikkofox/vk-wall-cleaner/core"
)

const confirmPage = "confirmModal"

var (
	basicStyle  = customStyle.Background(activeBackColor).Foreground(brightFrontColor)
	setupStyle  = customStyle.Background(brightFrontColor).Foreground(activeBackColor)
	filterModes = map[string]core.FilterMode{"Skip RegEx": core.DefaultFilterMode, "Only RegEx": core.FilterOnlyRe}
	filterDesc  = map[core.FilterMode]string{
		core.FilterOnlyRe: "[::u]Удалять только посты[::-], которые совпадают с регулярным выражением",
		core.FilterSkipRe: "[::u]Пропускать посты[::-], которые совпадают с регулярным выражением",
	}
)

// WallPane displays available VK walls. Personal page and groups
type WallPane struct {
	*tview.Flex
	list         *tview.List
	help         *tview.Grid
	deepCheckbox *tview.Checkbox
	autoRetry    *tview.Checkbox
	modeSelect   *tview.DropDown
	regexFilter  *tview.InputField
	hint         *tview.TextView
}

// newWallPane initializes and configures WallPane
func newWallPane(config *core.Config) *WallPane {
	pane := WallPane{
		Flex:         tview.NewFlex().SetDirection(tview.FlexRow),
		list:         tview.NewList(),
		help:         makeWallHelp(),
		deepCheckbox: tview.NewCheckbox(),
		autoRetry:    tview.NewCheckbox(),
		modeSelect:   tview.NewDropDown(),
		regexFilter:  tview.NewInputField(),
		hint:         tview.NewTextView(),
	}

	// WallPane setup
	pane.configureList()
	pane.configureHintMessage()

	pane.
		AddItem(blank, 2, 0, false).
		AddItem(pane.makeSettingsRow(config.Cleaner), 3, 0, false).
		AddItem(pane.hint, 0, 1, false)

	pane.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		switch event.Key() {
		case tcell.KeyBacktab:
			App.UI.TView.SetFocus(App.UI.mainPane.tokens.list)
			return nil
		}
		return event
	})

	pane.SetBorder(true).SetTitle("[::u]W[::-]alls")

	return &pane
}

func makeWallHelp() *tview.Grid {
	return tview.NewGrid().
		SetColumns(0, 0, 0, 0).
		SetRows(0).
		AddItem(tview.NewTextView().SetText("Глубокая очистка: c").SetTextAlign(tview.AlignCenter), 0, 0, 1, 1, 0, 20, false).
		AddItem(tview.NewTextView().SetText("Автоповтор: a").SetTextAlign(tview.AlignCenter), 0, 1, 1, 1, 0, 20, false).
		AddItem(tview.NewTextView().SetText("Режим очистки: m").SetTextAlign(tview.AlignCenter), 0, 2, 1, 1, 0, 20, false).
		AddItem(tview.NewTextView().SetText("RegEx: INS / i").SetTextAlign(tview.AlignCenter), 0, 3, 1, 1, 0, 20, false)
}

func (pane *WallPane) makeSettingsRow(config *core.CleanerConfig) *tview.Flex {
	pane.configureDeepClean(config.DeepClean)
	pane.configureAutoRetry(config.AutoRetry)
	pane.configureMode(int(config.CleanMode))
	pane.configureRegEx(config.RegexStr)
	row := tview.NewFlex().
		SetDirection(tview.FlexColumn).
		AddItem(pane.deepCheckbox, 22, 1, false).
		AddItem(pane.autoRetry, 17, 1, false).
		AddItem(pane.modeSelect, 28, 1, false)
	return tview.NewFlex().SetDirection(tview.FlexRow).
		AddItem(row, 1, 1, false).
		AddItem(blank, 1, 1, false).
		AddItem(pane.regexFilter, 1, 1, false)
}

// Auto Retry checkbox

func (pane *WallPane) configureAutoRetry(checked bool) {
	pane.autoRetry.
		SetChecked(checked).
		SetLabel("Автоповтор: ").
		SetFieldBackgroundColor(activeBackColor).SetFieldTextColor(brightFrontColor)
	pane.autoRetry.SetChangedFunc(pane.handleCheckedAutoRetry)
	pane.autoRetry.SetDoneFunc(pane.handleAutoRetryDone)
}

func (pane *WallPane) handleCheckedAutoRetry(checked bool) {
	msg := "Включен [yellow]автоповтор[-]. При ограничениях [blue]VK Cleaner[-] повторит процесс через какое-то время"
	if !checked {
		msg = "Выключен [yellow]автоповтор[-]. При количественных ограничениях очистка остановится"
	}
	App.UI.status.showForSeconds(msg, 5)
	App.Cleaner.SetAutoRetry(checked)
}

func (pane *WallPane) handleAutoRetryDone(key tcell.Key) {
	switch key {
	case tcell.KeyTab:
		App.UI.TView.SetFocus(pane.modeSelect)
	case tcell.KeyBacktab:
		App.UI.TView.SetFocus(pane.deepCheckbox)
	case tcell.KeyEsc:
		App.UI.TView.SetFocus(pane.list)
	}
}

// Deep Clean checkbox

func (pane *WallPane) configureDeepClean(checked bool) {
	pane.deepCheckbox.
		SetLabel("Глубокая очистка: ").
		SetFieldBackgroundColor(activeBackColor).SetFieldTextColor(brightFrontColor).
		SetChecked(checked)
	pane.deepCheckbox.SetChangedFunc(pane.handleCheckedDeepClean)
	pane.deepCheckbox.SetDoneFunc(pane.handleDeepCleanDone)
}

func (pane *WallPane) handleCheckedDeepClean(checked bool) {
	msg := "Включен режим [yellow]глубокой очистки[-]. [blue]VK Cleaner[-] дополнительно удаляет вложения к посту: фото, видео, документы"
	if !checked {
		msg = "Выключен режим [yellow]глубокой очистки[-]. [blue]VK Cleaner[-] удаляет только пост"
	}
	App.UI.status.showForSeconds(msg, 5)
	App.Cleaner.SetDeepClean(checked)
}

func (pane *WallPane) handleDeepCleanDone(key tcell.Key) {
	switch key {
	case tcell.KeyTab:
		App.UI.TView.SetFocus(pane.autoRetry)
	case tcell.KeyEsc:
		App.UI.TView.SetFocus(pane.list)
	}
}

// Mode DropDown

func (pane *WallPane) configureMode(option int) {
	var texts []string
	for k := range filterModes {
		texts = append(texts, k)
	}
	pane.modeSelect.
		SetListStyles(basicStyle, setupStyle).
		SetFieldTextColor(activeBackColor).SetFieldBackgroundColor(brightFrontColor)
	pane.modeSelect.SetLabel("Режим очистки: ").SetOptions(texts, nil).SetCurrentOption(option)
	pane.modeSelect.SetSelectedFunc(pane.handleModeSelected).SetDoneFunc(pane.handleModeDone)
}

func (pane *WallPane) handleModeSelected(text string, _ int) {
	App.UI.status.showForSeconds(fmt.Sprintf("Выбран режим: [yellow]%s[-]. %s", text, filterDesc[filterModes[text]]), 5)
	App.Cleaner.SetMode(filterModes[text])
}

func (pane *WallPane) handleModeDone(key tcell.Key) {
	switch key {
	case tcell.KeyTab:
		App.UI.TView.SetFocus(pane.regexFilter)
	case tcell.KeyBacktab:
		App.UI.TView.SetFocus(pane.autoRetry)
	case tcell.KeyEsc:
		App.UI.TView.SetFocus(pane.list)
	}
}

// RegEx input field

func (pane *WallPane) configureRegEx(regex string) {
	style := basicStyle
	if regex != "" {
		if err := App.Cleaner.SetFilter(regex); err != nil {
			regex = ""
		} else {
			style = setupStyle
		}
	}
	pane.regexFilter.SetLabel("RegEx: ").
		SetText(regex).
		SetLabelColor(labelFrontColor).
		SetPlaceholder(" ").
		SetFieldStyle(style).SetPlaceholderStyle(customStyle.Background(inactiveBackColor)).
		SetDoneFunc(pane.handleRegexDone).SetChangedFunc(pane.handleRegexChanged)
}

func (pane *WallPane) handleRegexChanged(_ string) {
	pane.regexFilter.SetFieldStyle(basicStyle)
}

func (pane *WallPane) handleRegexDone(key tcell.Key) {
	{
		switch key {
		case tcell.KeyEnter:
			err := App.Cleaner.SetFilter(pane.regexFilter.GetText())
			if err != nil {
				errorInInputField(pane.regexFilter, 5)
				App.UI.status.showForSeconds("[red::b]RegEx фильтр содержит ошибки", 5)
				return
			}

			App.UI.status.showForSeconds("[green::]RegEx фильтр установлен", 5)
			App.UI.TView.SetFocus(pane.list)
			pane.regexFilter.SetFieldStyle(setupStyle)
		case tcell.KeyEsc, tcell.KeyDown, tcell.KeyTab:
			App.UI.TView.SetFocus(pane.list)
		case tcell.KeyBacktab:
			App.UI.TView.SetFocus(pane.modeSelect)
		}
	}
}

// Wall List

func (pane *WallPane) configureList() {
	pane.list.ShowSecondaryText(false).SetInputCapture(pane.handleListShortcuts).SetBorder(true)
}

func (pane *WallPane) handleListShortcuts(event *tcell.EventKey) *tcell.EventKey {
	switch unicode.ToLower(event.Rune()) {
	case 'j':
		pane.list.SetCurrentItem(pane.list.GetCurrentItem() + 1)
		return nil
	case 'k':
		pane.list.SetCurrentItem(pane.list.GetCurrentItem() - 1)
		return nil
	case 'i':
		App.UI.TView.SetFocus(pane.regexFilter)
		return nil
	case 'a':
		checked := !pane.autoRetry.IsChecked()
		pane.handleCheckedAutoRetry(checked)
		pane.autoRetry.SetChecked(checked)
		return nil
	case 'c':
		checked := !pane.deepCheckbox.IsChecked()
		pane.handleCheckedDeepClean(checked)
		pane.deepCheckbox.SetChecked(checked)
		return nil
	case 'm':
		idx, _ := pane.modeSelect.GetCurrentOption()
		if idx > 0 {
			pane.modeSelect.SetCurrentOption(0)
			return nil
		}
		pane.modeSelect.SetCurrentOption(1)
		return nil
	}

	switch event.Key() {
	case tcell.KeyInsert:
		App.UI.TView.SetFocus(pane.regexFilter)
		return nil
	case tcell.KeyLeft:
		App.UI.TView.SetFocus(App.UI.mainPane.tokens.list)
		return nil
	}

	return event
}

func (pane *WallPane) showList() {
	pane.
		RemoveItem(pane.hint).
		AddItem(pane.list, 0, 1, true).
		AddItem(pane.help, 1, 0, false)
}

func (pane *WallPane) hideList() {
	pane.
		RemoveItem(pane.list).RemoveItem(pane.help).
		AddItem(pane.hint, 0, 1, false)
}

func (pane *WallPane) clearList() {
	pane.list.Clear()
	pane.hideList()
}

func (pane *WallPane) loadWalls() {
	u, err := App.Cleaner.GetUser()
	if err != nil {
		App.UI.status.showForSeconds(errorf("Ошибка получения информации о пользователе", err), 5)
		return
	}

	pane.clearList()
	pane.addWallToList(*u.ToWall())

	groups, err := App.Cleaner.LoadGroups()
	if err != nil {
		App.UI.status.showForSeconds(errorf("Ошибка получения групп", err), 5)
		return
	}
	for _, g := range *groups {
		pane.addWallToList(*g.ToWall())
	}
	App.UI.mainPane.walls.showList()
	App.UI.TView.SetFocus(App.UI.mainPane.walls)
}

func (pane *WallPane) addWallToList(wall core.Wall) *tview.List {
	text := fmt.Sprintf("[gray]%12s[-] %s", fmt.Sprintf("[%d]", wall.ID), wall.Name)
	return pane.list.AddItem(text, "", 0, func(wall *core.Wall) func() {
		return func() { pane.clearWall(wall) }
	}(&wall))
}

func (pane *WallPane) clearWall(wall *core.Wall) {
	cancel := func() {
		App.UI.mainPane.Pages.RemovePage(confirmPage)
		App.UI.TView.SetFocus(pane.list)
	}
	ok := func() {
		App.UI.status.showForSeconds(
			fmt.Sprintf("Очистка стены: [gray][%d] [yellow]%s[-]", wall.ID, wall.Name), 3,
		)
		App.UI.hideMainPane()
		ctx, _ := App.UI.ctxWrapper.Get()
		go func(ctx context.Context) {
			if err := App.Cleaner.ClearWall(ctx, App.UI.mainPane.log.queueMessage, wall); err != nil {
				App.UI.status.showForSeconds(errorf("Ошибка очистки стены", err), 5)
			}
			App.UI.TView.QueueUpdateDraw(func() {
				App.UI.showMainPane()
				cancel()
			})
		}(ctx)
	}

	modal := confirmModal(
		fmt.Sprintf(
			"Очистить стену\n[gray][%d] [yellow]%s[-]",
			wall.ID, wall.Name,
		), ok, cancel,
	)
	App.UI.mainPane.Pages.AddAndSwitchToPage(confirmPage, modal, true)
}

// Hint message

func (pane *WallPane) configureHintMessage() {
	pane.hint.
		SetTextColor(labelFrontColor).SetTextAlign(tview.AlignLeft).
		SetDynamicColors(true).SetBorderPadding(1, 0, 1, 1)
	logo := `
╦  ╦╦╔═  ╔═╗┬  ┌─┐┌─┐┌┐┌┌─┐┬─┐
╚╗╔╝╠╩╗  ║  │  ├┤ ├─┤│││├┤ ├┬┘
 ╚╝ ╩ ╩  ╚═╝┴─┘└─┘┴ ┴┘└┘└─┘┴└─
`
	pane.hint.SetText(
		fmt.Sprintf(
			"%s\n"+
				"[white::b]Горячие клавиши:[-::-]\n"+
				"\t[green::]Ctrl-A[-::] - Панель со списком токенов\n"+
				"\t[green::]Ctrl-W[-::] - Панель со списком стен\n"+
				"\t[green::]Ctrl-S[-::] - Сохранить настройки\n"+
				"\t[green::]Ctrl-R[-::] - Сбросить настройки\n"+
				"\t[green::]Ctrl-X[-::] - Прервать выполнение\n"+
				"\t[green::]Ctrl-C[-::] - Выход\n\n"+
				"[white::b]Автоповтор[-::-] - при ограничениях API, приложение будет пробовать снова через какое-то время\n"+
				"[white::b]Режимы очистки:[-::-]\n"+
				"\t[blue::]Skip RegEx      [-::] - %s\n"+
				"\t[blue::]Only RegEx      [-::] - %s\n"+
				"\t[blue::]Глубокая очистка[-::] - Дополнительно удаляет вложения к посту\n",
			logo, filterDesc[core.FilterSkipRe], filterDesc[core.FilterOnlyRe],
		),
	)

}
