package ui

import (
	"fmt"

	"github.com/rivo/tview"
)

// newTitleBar initializes and configures upper bar with app name and version
func newTitleBar(version string) *tview.Flex {
	title := tview.NewTextView().
		SetText("[blue::b]VK Wall Cleaner[::-] - Удаляет посты из ВК вместо тебя").
		SetDynamicColors(true)

	versionInfo := tview.NewTextView().
		SetText(fmt.Sprintf("[gray::d]Version: %s", version)).SetTextAlign(tview.AlignRight).
		SetDynamicColors(true)

	return tview.NewFlex().
		AddItem(title, 0, 2, false).
		AddItem(versionInfo, 0, 1, false)
}
