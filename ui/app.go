package ui

import (
	"gitlab.com/nikkofox/vk-wall-cleaner/core"
)

// App global App variable.
var App *VKCleanerApp

// VKCleanerApp main application struct.
type VKCleanerApp struct {
	VK      *core.VK
	UI      *UserInterface
	Cleaner *core.Cleaner
}

// InitApp creates a global App struct
func InitApp(version string) *VKCleanerApp {
	config := core.NewConfig()
	vk := core.NewVK(config.VK)
	App = &VKCleanerApp{
		VK:      vk,
		Cleaner: core.NewCleaner(vk, config.Cleaner),
	}
	App.UI = newUI(version, config)
	return App
}
