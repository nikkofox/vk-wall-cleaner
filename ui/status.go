package ui

import (
	"time"

	"github.com/rivo/tview"
)

// StatusBar displays hints and messages at the bottom of app
type StatusBar struct {
	*tview.Pages
	message *tview.TextView
}

// Name of page keys
const (
	defaultPage = "default"
	messagePage = "message"
)

// Used to skip queued restore of statusBar
var inQueue = 0

func newStatusBar() *StatusBar {
	statusBar := &StatusBar{
		Pages:   tview.NewPages(),
		message: tview.NewTextView().SetDynamicColors(true),
	}

	statusBar.AddPage(messagePage, statusBar.message, true, true)
	statusBar.AddPage(defaultPage,
		tview.NewGrid().SetColumns(0, 0, 0, 0, 0).SetRows(0).
			AddItem(tview.NewTextView().SetText("Навигация по списку: ↓,↑ / j,k"), 0, 0, 1, 1, 0, 0, false).
			AddItem(tview.NewTextView().SetText("Вперёд: Tab").SetTextAlign(tview.AlignCenter), 0, 1, 1, 1, 0, 0, false).
			AddItem(tview.NewTextView().SetText("Назад: Shift-Tab").SetTextAlign(tview.AlignCenter), 0, 2, 1, 1, 0, 0, false).
			AddItem(tview.NewTextView().SetText("Прервать: Ctrl-X").SetTextAlign(tview.AlignCenter), 0, 3, 1, 1, 0, 0, false).
			AddItem(tview.NewTextView().SetText("Выход: Ctrl-C").SetTextAlign(tview.AlignRight), 0, 4, 1, 1, 0, 0, false),
		true,
		true,
	)

	return statusBar
}

func (bar *StatusBar) restore() {
	App.UI.TView.QueueUpdateDraw(func() {
		bar.SwitchToPage(defaultPage)
	})
}

func (bar *StatusBar) showForSeconds(message string, timeout int) {
	bar.message.SetText(message)
	App.UI.mainPane.log.message(message)
	bar.SwitchToPage(messagePage)
	inQueue++

	go func() {
		time.Sleep(time.Second * time.Duration(timeout))

		// Apply restore only if this is the last pending restore
		if inQueue == 1 {
			bar.restore()
		}
		inQueue--
	}()
}
