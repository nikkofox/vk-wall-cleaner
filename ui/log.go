package ui

import (
	"fmt"
	"time"

	"github.com/rivo/tview"
)

// logField displays logs
type logField struct {
	*tview.TextView
}

func newLogField() *logField {
	f := logField{TextView: tview.NewTextView()}
	f.SetMaxLines(300).SetDynamicColors(true)
	f.SetBorder(true)
	return &f
}

func (l *logField) message(text string) {
	t := time.Now().Format("15:04:05.00")
	_, err := l.Write([]byte(fmt.Sprintf("[-:-:-]\n[yellow]%s[-] - %s", t, text)))
	if err != nil {
		return
	}
}

func (l *logField) queueMessage(text string) {
	t := time.Now().Format("15:04:05.00")
	App.UI.TView.QueueUpdateDraw(func() {
		_, err := l.Write([]byte(fmt.Sprintf("[-:-:-]\n[yellow]%s[-] - %s", t, text)))
		if err != nil {
			return
		}
	})
}
