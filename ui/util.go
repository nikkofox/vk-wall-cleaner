package ui

import (
	"time"

	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
)

const (
	brightFrontColor  = tcell.ColorWhite
	labelFrontColor   = tcell.ColorYellow
	activeBackColor   = tcell.ColorDarkBlue
	modalBackColor    = tcell.ColorDarkBlue
	inactiveBackColor = tcell.ColorDarkCyan
	errorBackColor    = tcell.ColorRed
)

var customStyle tcell.Style

var blank = tview.NewTextView()

// errorInputField changes input field background color to red for the specified numbers of sec
func errorInInputField(field *tview.InputField, seconds int) {
	style := field.GetFieldStyle()

	go func() {
		field.SetFieldStyle(customStyle.Background(errorBackColor))
		time.Sleep(time.Second * time.Duration(seconds))
		App.UI.TView.QueueUpdateDraw(func() {
			field.SetFieldStyle(style)
		})
	}()
}

func errorf(message string, err error) string {
	return "[red]" + message + ": [blue::b]" + err.Error()
}
