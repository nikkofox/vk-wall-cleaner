BINARY_NAME=vk_wl_cleaner
PACKAGE_VERSION=${CI_COMMIT_TAG}
BUILD_DIR=build

build:
	CGO_ENABLED=0 GOARCH=amd64 GOOS=linux go build -o "${BUILD_DIR}/${BINARY_NAME}${PACKAGE_VERSION}-linux-amd64"
	CGO_ENABLED=0 GOARCH=arm64 GOOS=linux go build -o "${BUILD_DIR}/${BINARY_NAME}${PACKAGE_VERSION}-linux-arm64"
	CGO_ENABLED=0 GOARCH=amd64 GOOS=windows go build -o "${BUILD_DIR}/${BINARY_NAME}${PACKAGE_VERSION}-win-amd64.exe"
	CGO_ENABLED=0 GOARCH=arm64 GOOS=windows go build -o "${BUILD_DIR}/${BINARY_NAME}${PACKAGE_VERSION}-win-arm64.exe"
	CGO_ENABLED=0 GOARCH=amd64 GOOS=darwin go build -o "${BUILD_DIR}/${BINARY_NAME}${PACKAGE_VERSION}-osx-amd64"
	CGO_ENABLED=0 GOARCH=arm64 GOOS=darwin go build -o "${BUILD_DIR}/${BINARY_NAME}${PACKAGE_VERSION}-osx-arm64"
	chmod +x build/*

clean:
	go clean
	rm -r ${BUILD_DIR}