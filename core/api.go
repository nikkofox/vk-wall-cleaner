package core

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"time"
)

const (
	Version   = "5.131"
	MethodURL = "https://api.vk.com/method/"
)

// VK the client for this application.
type VK struct {
	Client        *http.Client
	AccessTokens  map[string]*AccessToken
	ActiveTokenId string
	config        *VKConfig
}

func NewVK(config *VKConfig) *VK {
	return &VK{
		Client:       &http.Client{},
		AccessTokens: map[string]*AccessToken{},
		config:       config,
	}
}

func (vk *VK) AddAccessToken(token string) *AccessToken {
	vk.config.Tokens = append(vk.config.Tokens, token)
	t := NewToken(token)
	vk.AccessTokens[t.Id] = t
	return t
}

func (vk *VK) RemoveAccessToken(tokenId string) AccessToken {
	if len(vk.AccessTokens) == 0 {
		return AccessToken{}
	}
	t := *vk.AccessTokens[tokenId]
	delete(vk.AccessTokens, tokenId)
	return t
}

func (vk *VK) ActivateAccessToken(tokenId string) *AccessToken {
	vk.ActiveTokenId = tokenId
	return vk.AccessTokens[tokenId]
}

func (vk *VK) getActiveToken() string {
	return vk.AccessTokens[vk.ActiveTokenId].Token
}

type Response struct {
	Response      json.RawMessage `json:"response"`
	Error         Error           `json:"error"`
	ExecuteErrors json.RawMessage `json:"execute_errors"`
}

func buildQuery(sliceParams ...Params) url.Values {
	query := url.Values{}

	for _, params := range sliceParams {
		for key, value := range params {
			query.Set(key, value)
		}
	}

	return query
}

func (vk *VK) defaultHandler(method string, sliceParams ...Params) (Response, error) {
	u := MethodURL + method
	query := buildQuery(sliceParams...)

	var err error
	response := Response{}
	for attempt := 0; attempt < 3; attempt++ {
		start := time.Now()
		rawBody := bytes.NewBufferString(query.Encode())

		req, err := http.NewRequest("POST", u, rawBody)
		if err != nil {
			continue
		}

		req.Header.Set("Authorization", "Bearer "+vk.getActiveToken())
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

		resp, err := vk.Client.Do(req)
		if err != nil {
			continue
		}

		if err = json.NewDecoder(resp.Body).Decode(&response); err != nil {
			_ = resp.Body.Close()
			continue
		}

		_ = resp.Body.Close()
		if elapsed := time.Since(start); elapsed <= time.Second/3 { // 3 rps
			time.Sleep(time.Second/3 - elapsed)
		}
		break
	}
	return response, err
}

func (vk *VK) Request(method string, obj any, sliceParams ...Params) error {
	reqParams := Params{
		"access_token": vk.getActiveToken(),
		"v":            Version,
	}

	sliceParams = append(sliceParams, reqParams)

	resp, err := vk.defaultHandler(method, sliceParams...)
	if err != nil {
		return err
	}
	if resp.Error.Code != 0 {
		return &resp.Error
	}
	return json.Unmarshal(resp.Response, &obj)
}

func (vk *VK) execute(code string, obj any) error {
	return vk.Request("execute", &obj, Params{"code": code})
}

func (vk *VK) makeRemoveScript(method string, itemType itemType, items ...*removableItem) string {
	ps := "["
	for _, i := range items {
		ps += "[" + toStr(i.id) + ", " + toStr(i.ownerId) + "], "
	}
	ps += "]"
	return fmt.Sprintf(`var items = %s;
var response = [];
while(items.length > 0) {
	var p=items.pop();
	response.push(API.%s({"%s":p[0], "owner_id":p[1]}));
};
return response;
`, ps, method, itemType.paramID())
}
