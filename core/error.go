package core

// Error struct VK
type Error struct {
	Code       int    `json:"error_code"`
	Subcode    int    `json:"error_subcode"`
	Message    string `json:"error_msg"`
	Text       string `json:"error_text"`
	CaptchaSID string `json:"captcha_sid"`
	CaptchaImg string `json:"captcha_img"`

	ConfirmationText string `json:"confirmation_text"`

	RedirectURI string `json:"redirect_uri"`
}

// Error returns the message of an Error
func (e Error) Error() string {
	return e.Message
}
