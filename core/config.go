package core

import (
	"crypto/sha256"
	"encoding/json"
	"os"
	"path/filepath"
)

var configFilePath = filepath.Join(getConfigDir(), "config.json")

type CleanerConfig struct {
	RegexStr  string     `json:"regex_str"`
	CleanMode FilterMode `json:"clean_mode"`
	DeepClean bool       `json:"deep_clean"`
	AutoRetry bool       `json:"auto_retry"`
}

type VKConfig struct {
	Tokens []string `json:"tokens"`
}

type Config struct {
	Cleaner *CleanerConfig `json:"cleaner_config"`
	VK      *VKConfig      `json:"vk_config"`
	secret  []byte
}

func NewConfig() *Config {
	name, err := os.Hostname()
	if err != nil {
		name = "vk_wall_cleaner"
	}

	key := sha256.Sum256([]byte(name))
	return &Config{
		Cleaner: &CleanerConfig{
			CleanMode: DefaultFilterMode,
			DeepClean: false,
			AutoRetry: false,
		},
		VK:     &VKConfig{Tokens: nil},
		secret: key[:],
	}
}

func (c *Config) SaveConfig() error {
	config, err := json.Marshal(c)
	if err != nil {
		return err
	}

	encoded, err := encrypt(c.secret, config)
	if err != nil {
		return err
	}

	return os.WriteFile(configFilePath, encoded, 0600)
}

func (c *Config) LoadConfig() error {
	config, err := os.ReadFile(configFilePath)
	if err != nil {
		return err
	}

	decoded, err := decrypt(c.secret, config)
	if err != nil {
		return err
	}

	return json.Unmarshal(decoded, c)
}

func (c *Config) ResetConfig() error {
	if err := os.Remove(configFilePath); err != nil {
		return err
	}
	return nil
}

func getConfigDir() string {
	configDir, err := os.UserConfigDir()
	if err != nil {
		configDir, err = os.UserHomeDir()
		if err != nil {
			configDir = ""
		}
	}

	filePath := filepath.Join(configDir, "vk_wall_cleaner")
	if err = os.MkdirAll(filePath, 0700); err != nil {
		panic("Нет доступа для записи файла с настройками" + filePath)
	}
	return filePath
}
