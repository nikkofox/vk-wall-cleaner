package core

import (
	"context"
	"time"
)

// ContextWrapper contains basic methods for context and cancel management
type ContextWrapper struct {
	context context.Context
	cancel  context.CancelFunc
}

// NewContextWrapper creates new ContextWrapper
func NewContextWrapper() *ContextWrapper {
	c := &ContextWrapper{}
	c.Reset()
	return c
}

// Get returns declared context and cancel func from ContextWrapper
func (c *ContextWrapper) Get() (context.Context, context.CancelFunc) {
	return c.context, c.cancel
}

// Reset resets the state of the cancel func and sets a new context
func (c *ContextWrapper) Reset() {
	c.context, c.cancel = context.WithCancel(context.Background())
}

// ToCancel calls the cancel func
func (c *ContextWrapper) ToCancel() {
	c.cancel()
}

// ToCancelWithReset calls the cancel func and sets a new context
func (c *ContextWrapper) ToCancelWithReset() {
	c.ToCancel()
	c.Reset()
}

// SleepContext allows to abort sleep by calling the context cancel func
func SleepContext(ctx context.Context, duration time.Duration) {
	select {
	case <-ctx.Done():
	case <-time.After(duration):
	}
}

// IsContextCancel returns true if the operation is canceled, otherwise false
func IsContextCancel(ctx context.Context) bool {
	select {
	case <-ctx.Done():
		return true
	default:
		return false
	}
}
