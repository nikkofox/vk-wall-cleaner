package core

import "strconv"

// toStr shorthand for strconv.Itoa method
func toStr(i int) string {
	return strconv.Itoa(i)
}

// sum calculates sum of the elements in array
func sum(arr []int) int {
	r := 0
	for i := 0; i < len(arr); i++ {
		r += arr[i]
	}
	return r
}

// values returns values from map
func values[M ~map[K]V, K comparable, V any](m M) []V {
	r := make([]V, 0, len(m))
	for _, v := range m {
		r = append(r, v)
	}
	return r
}
