package core

import (
	"context"
	"regexp"
	"strings"
	"time"
)

const (
	wallCount      = 100 // max for request
	autoRetryLimit = 10
	retryDelay     = 30 // minutes
)

type Cleaner struct {
	vk          *VK
	regexFilter *regexp.Regexp
	config      *CleanerConfig
}

func NewCleaner(vk *VK, config *CleanerConfig) *Cleaner {
	return &Cleaner{vk: vk, regexFilter: nil, config: config}
}

func (c *Cleaner) SetFilter(regex string) (err error) {
	if regex == "" {
		c.regexFilter = nil
		c.config.RegexStr = regex
		return
	}
	if c.regexFilter, err = regexp.Compile(regex); err == nil {
		c.config.RegexStr = regex
	}
	return
}

func (c *Cleaner) matchFilter(text string) bool {
	if c.regexFilter == nil {
		return true
	}
	match := c.regexFilter.MatchString(text)
	if c.config.CleanMode == FilterOnlyRe {
		return match // FilterOnlyRe, return true if the text matches the RegEx
	}
	return !match
}

func (c *Cleaner) SetMode(mode FilterMode) {
	c.config.CleanMode = mode
}

func (c *Cleaner) SetDeepClean(deep bool) {
	c.config.DeepClean = deep
}

func (c *Cleaner) SetAutoRetry(enable bool) {
	c.config.AutoRetry = enable
}

func (c *Cleaner) GetUser() (*User, error) {
	var users []User
	if err := c.vk.Request("users.get", &users, Params{}); err != nil {
		return nil, err
	}
	c.vk.AccessTokens[c.vk.ActiveTokenId].SetUser(&users[0])
	return &users[0], nil
}

func (c *Cleaner) LoadGroups() (*[]Group, error) {
	groupsGet := GroupsGetExtended{}
	err := c.vk.Request("groups.get", &groupsGet, Params{"filter": "moder", "extended": "1"})
	if err != nil {
		return nil, err
	}
	var group []Group
	for _, g := range groupsGet.Items {
		if !g.IsGroupDeactivated() {
			group = append(group, g)
		}
	}
	return &group, nil
}

func (c *Cleaner) loadWall(wall *Wall, count, offset int) error {
	w := WallGetExtended{}
	err := c.vk.Request(
		"wall.get", &w, Params{"owner_id": toStr(wall.OwnerID), "count": toStr(count), "offset": toStr(offset)},
	)
	wall.Posts = w.Items
	wall.Count = w.Count
	return err
}

func (c *Cleaner) isLimExceeded(ctx context.Context, log func(text string), attempt int) bool {
	log("[red]Текущий пользователь достиг количественного лимита VK API!")
	if c.config.AutoRetry && attempt < autoRetryLimit {
		log("[green]Повторная попытка #" + toStr(attempt+1) + " через " + toStr(retryDelay) + " мин")
		SleepContext(ctx, time.Minute*time.Duration(retryDelay))
		return false
	} else {
		log("[red]Попробуйте продолжить через какое-то время")
		return true
	}
}

func (c *Cleaner) ClearWall(ctx context.Context, log func(text string), wall *Wall) error {
	var prevItemCounter, itemCounter, postCounter, attempt, offset, repeat int
	for {
		repeat++
		err := c.loadWall(wall, wallCount, offset)
		if err != nil {
			return err
		}

		if len(wall.Posts) == 0 {
			log("[yellow]Записи со стены удалены, остаток: " + toStr(wall.Count))
			if c.config.DeepClean {
				log("[purple]Включен режим глубокой очистки, рекомендуется вручную проверить:")
				log("\tФотографии на стене сообщества:[blue] https://vk.com/album" + toStr(wall.OwnerID) + "_00")
				log("\tВидеозаписи сообщества:[blue] https://vk.com/video" + toStr(wall.OwnerID) + "_00")
				log("\tФайлы сообщества:[blue] https://vk.com/docs" + toStr(wall.OwnerID))
			}
			return nil
		}

		if prevItemCounter != itemCounter || postCounter != wall.Count {
			attempt = 0
		} else if repeat > 4 {
			if c.isLimExceeded(ctx, log, attempt) {
				return nil
			}
			attempt++
		}

		log("[white]Записей осталось: [blue]" + toStr(wall.Count))
		log("[white]Смещение по стене: [purple]" + toStr(offset))

		items, skipped := c.filterItems(wall.Posts...)

		log("[white]Получено записей: [yellow]" + toStr(len(wall.Posts)) +
			"[white], отфильтровано: [red]" + toStr(skipped) +
			"[white], к удалению: [green]" + toStr(len(wall.Posts)-skipped))

		if IsContextCancel(ctx) {
			log("[red]Выполнение прервано пользователем")
			return nil
		}

		prevItemCounter = itemCounter
		itemCounter = 0
		if len(items) > 0 {
			for _, v := range values(items) {
				itemCounter += len(v)
			}

			if err := c.clearChunkPosts(log, items); err != nil {
				return err
			}
		}

		postCounter = wall.Count
		if skipped == len(wall.Posts) {
			repeat = 0
			offset += skipped
		}
	}
}

func (c *Cleaner) filterItems(posts ...Post) (map[itemType][]*removableItem, int) {
	removableItems := map[itemType][]*removableItem{}
	remainder := len(posts)
	for _, p := range posts {
		if !c.matchFilter(p.Text) || !bool(p.CanDelete) {
			continue
		}
		remainder--
		if !c.config.DeepClean || p.Attachments == nil || len(p.Attachments) == 0 {
			removableItems[wall] = append(removableItems[wall], p.asItem())
			continue
		}
		skip := false
		for _, a := range p.Attachments {
			item := a.asItem()
			if item.ownerId == p.OwnerID || item.ownerId == c.vk.AccessTokens[c.vk.ActiveTokenId].User.ID {
				removableItems[item.itemType] = append(removableItems[item.itemType], item)
				continue
			}
			if !skip {
				remainder++
				skip = true
			}
		}
	}
	return removableItems, remainder
}

func (c *Cleaner) clearChunkPosts(log func(text string), items map[itemType][]*removableItem) error {
	var t []string
	for k, v := range items {
		log("Приступаю к удалению: [blue]" + string(k) + " - " + toStr(len(v)))
		result, err := c.execRemoveScript(k, v...)
		if err != nil {
			return err
		}

		t = append(t, string(k)+"[-] - [purple]"+toStr(sum(result))+"/"+toStr(len(v)))
	}
	log("[green]Удалено: " + strings.Join(t, "[green], ") + "\n")
	return nil
}

func (c *Cleaner) execRemoveScript(itemType itemType, items ...*removableItem) ([]int, error) {
	var responses []int
	var resp []int
	if len(items) > 25 {
		resp, err := c.execRemoveScript(itemType, items[25:]...)
		if err != nil {
			return responses, err
		}
		items = items[:25]
		responses = append(responses, resp...)
	}
	code := c.vk.makeRemoveScript(itemType.method()+".delete", itemType, items...)
	err := c.vk.execute(code, &resp)
	responses = append(responses, resp...)
	return responses, err
}

// TODO: add checkbox for slow mode
//func (c *Cleaner) removePost(post *Post) (int, error) {
//	var response int
//	err := c.vk.Request("wall.delete", &response, Params{"owner_id": toStr(post.OwnerID), "post_id": toStr(post.ID)})
//	return response, err
//}
//
//func (c *Cleaner) removeAttachment(attach *WallPostAttachment) {
//	switch {
//	case attach.Photo != Attachment{}:
//		c.removePhoto(&attach.Photo)
//	case attach.Doc != Attachment{}:
//		c.removeDocs(&attach.Doc)
//	case attach.Video != Attachment{}:
//		c.removeVideo(&attach.Video)
//	}
//}
//
//func (c *Cleaner) removePhoto(attach *Attachment) error {
//	var response int
//	err := c.vk.Request("photos.delete", &response, Params{"owner_id": toStr(attach.OwnerID), "photo_id": toStr(attach.ID)})
//	return err
//}
//
//func (c *Cleaner) removeVideo(attach *Attachment) error {
//	var response int
//	err := c.vk.Request("video.delete", &response, Params{"owner_id": toStr(attach.OwnerID), "video_id": toStr(attach.ID)})
//	return err
//}
//
//func (c *Cleaner) removeDocs(attach *Attachment) error {
//	var response int
//	err := c.vk.Request("docs.delete", &response, Params{"owner_id": toStr(attach.OwnerID), "doc_id": toStr(attach.ID)})
//	return err
//}
