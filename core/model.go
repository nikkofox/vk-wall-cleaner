package core

import (
	"bytes"

	"github.com/google/uuid"
)

// AccessToken struct for storing extended information about tokens
type AccessToken struct {
	Token string
	Alias string
	Id    string
	User  *User
}

// NewToken creates new AccessToken struct
func NewToken(token string) *AccessToken {
	return &AccessToken{
		Token: token,
		Alias: token[0:12] + "..." + token[len(token)-12:],
		Id:    uuid.NewString(),
	}
}

// SetUser adds user struct to token struct
func (t *AccessToken) SetUser(user *User) {
	t.User = user
}

// FilterMode wall cleaning filter modes
type FilterMode uint8

const (
	FilterSkipRe FilterMode = iota
	FilterOnlyRe
)
const DefaultFilterMode = FilterSkipRe

// Params pass in http request
type Params map[string]string

type itemType string

const (
	doc   itemType = "doc"
	photo itemType = "photo"
	video itemType = "video"
	wall  itemType = "wall"
)

func (i *itemType) paramID() string {
	if *i == wall {
		return "post_id"
	}
	return string(*i) + "_id"
}

func (i *itemType) method() string {
	switch *i {
	case doc:
		return string(*i) + "s"
	case photo:
		return string(*i) + "s"
	default:
		return string(*i)
	}
}

// BoolInt type implements int to bool conversion. 1 -> true, otherwise return false
type BoolInt bool

func (b *BoolInt) UnmarshalJSON(data []byte) error {
	switch {
	case bytes.Equal(data, []byte("1")):
		*b = true
	default:
		*b = false
	}
	return nil
}

type GroupsGetExtended struct {
	Count int     `json:"count"`
	Items []Group `json:"items"`
}

type Group struct {
	ID          int    `json:"id"`                    // Community ID
	Name        string `json:"name"`                  // Community name
	ScreenName  string `json:"screen_name"`           // Domain of the community page
	Deactivated string `json:"deactivated,omitempty"` // Information whether community is banned
}

func (g *Group) ToWall() *Wall {
	return &Wall{
		ID:      g.ID,
		OwnerID: -g.ID,
		Name:    g.Name,
	}
}

func (g *Group) IsGroupDeactivated() bool {
	switch g.Deactivated {
	case "deleted", "banned":
		return true
	default:
		return false
	}
}

type User struct {
	ID        int    `json:"id"`         // User's ID
	FirstName string `json:"first_name"` // User's first name
	LastName  string `json:"last_name"`  // User's last name
}

func (u *User) ToWall() *Wall {
	return &Wall{
		ID:      u.ID,
		OwnerID: u.ID,
		Name:    u.FirstName + " " + u.LastName + " (Личная страница)",
	}
}

// Wall contains the main fields
type Wall struct {
	ID      int
	OwnerID int
	Name    string
	Posts   []Post
	Count   int
}

type WallGetExtended struct {
	Count int    `json:"count"`
	Items []Post `json:"items"`
}

type Post struct {
	ID          int                  `json:"id"`
	OwnerID     int                  `json:"owner_id"`
	CanDelete   BoolInt              `json:"can_delete"`
	Text        string               `json:"text"`
	Attachments []WallPostAttachment `json:"attachments,omitempty"`
}

func (p *Post) asItem() *removableItem {
	return &removableItem{
		id:       p.ID,
		ownerId:  p.OwnerID,
		itemType: wall,
	}
}

type WallPostAttachment struct {
	Video Attachment `json:"video"`
	Photo Attachment `json:"photo"`
	Doc   Attachment `json:"doc"`
}

func (wa *WallPostAttachment) asItem() *removableItem {
	itemType := photo
	attach := wa.Photo
	switch {
	case wa.Doc != Attachment{}:
		itemType = doc
		attach = wa.Doc
	case wa.Video != Attachment{}:
		itemType = video
		attach = wa.Video
	}
	return &removableItem{
		id:       attach.ID,
		ownerId:  attach.OwnerID,
		itemType: itemType,
	}
}

type Attachment struct {
	ID        int    `json:"id"`                   // Attachment's ID
	OwnerID   int    `json:"owner_id"`             // Attachment's owner ID
	AccessKey string `json:"access_key,omitempty"` // Attachment's access key
}

type removableItem struct {
	id       int
	ownerId  int
	itemType itemType
}
