#!/bin/sh

export CGO_ENABLED=0
export BINARY_NAME=vk_wl_cleaner
export BUILD_DIR=build
export PACKAGE_VERSION=${CI_COMMIT_TAG}

GOARCH=amd64 GOOS=linux go build -o "${BUILD_DIR}/${BINARY_NAME}${PACKAGE_VERSION}-linux-amd64"
GOARCH=arm64 GOOS=linux go build -o "${BUILD_DIR}/${BINARY_NAME}${PACKAGE_VERSION}-linux-arm64"
GOARCH=amd64 GOOS=windows go build -o "${BUILD_DIR}/${BINARY_NAME}${PACKAGE_VERSION}-win-amd64.exe"
GOARCH=arm64 GOOS=windows go build -o "${BUILD_DIR}/${BINARY_NAME}${PACKAGE_VERSION}-win-arm64.exe"
GOARCH=amd64 GOOS=darwin go build -o "${BUILD_DIR}/${BINARY_NAME}${PACKAGE_VERSION}-osx-amd64"
GOARCH=arm64 GOOS=darwin go build -o "${BUILD_DIR}/${BINARY_NAME}${PACKAGE_VERSION}-osx-arm64"
chmod +x build/*
